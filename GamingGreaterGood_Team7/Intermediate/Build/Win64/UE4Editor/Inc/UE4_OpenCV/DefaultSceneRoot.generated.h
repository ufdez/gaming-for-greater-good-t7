// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4_OPENCV_DefaultSceneRoot_generated_h
#error "DefaultSceneRoot.generated.h already included, missing '#pragma once' in DefaultSceneRoot.h"
#endif
#define UE4_OPENCV_DefaultSceneRoot_generated_h

#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_SPARSE_DATA
#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_RPC_WRAPPERS
#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDefaultSceneRoot(); \
	friend struct Z_Construct_UClass_UDefaultSceneRoot_Statics; \
public: \
	DECLARE_CLASS(UDefaultSceneRoot, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4_OpenCV"), NO_API) \
	DECLARE_SERIALIZER(UDefaultSceneRoot)


#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUDefaultSceneRoot(); \
	friend struct Z_Construct_UClass_UDefaultSceneRoot_Statics; \
public: \
	DECLARE_CLASS(UDefaultSceneRoot, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4_OpenCV"), NO_API) \
	DECLARE_SERIALIZER(UDefaultSceneRoot)


#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDefaultSceneRoot(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDefaultSceneRoot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDefaultSceneRoot); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDefaultSceneRoot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDefaultSceneRoot(UDefaultSceneRoot&&); \
	NO_API UDefaultSceneRoot(const UDefaultSceneRoot&); \
public:


#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDefaultSceneRoot(UDefaultSceneRoot&&); \
	NO_API UDefaultSceneRoot(const UDefaultSceneRoot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDefaultSceneRoot); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDefaultSceneRoot); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDefaultSceneRoot)


#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_PRIVATE_PROPERTY_OFFSET
#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_10_PROLOG
#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_PRIVATE_PROPERTY_OFFSET \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_SPARSE_DATA \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_RPC_WRAPPERS \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_INCLASS \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_PRIVATE_PROPERTY_OFFSET \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_SPARSE_DATA \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_INCLASS_NO_PURE_DECLS \
	GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4_OPENCV_API UClass* StaticClass<class UDefaultSceneRoot>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GamingGreaterGood_Team7_Source_UE4_OpenCV_Public_DefaultSceneRoot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
