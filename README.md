# gaming-for-greater-good-T7

Eye and hand tracking games are made for children with specials needs. Uses standard web camera and OpenCV for eye-tracking and Leap Motion Controller for hand-tracking.

kudos to @trishume for the eyetracking implementation  https://github.com/trishume/eyeLike

kudos to @Dukhart for the OpenCV integration  into UE4 https://github.com/Dukhart/UE4_OpenCV

Please use Readme.docx to setup project and handle any issues when using the project.
